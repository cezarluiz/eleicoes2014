var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Slug = require('mongoose-friendly');
var autoIncrement = require('mongoose-auto-increment');
var db = mongoose.createConnection('mongodb://localhost:27017/eleicoes2014');

/**
 * Opções
 */
var options = {
	versionKey: false
}

autoIncrement.initialize(db);

/**
 * Schemas
 */
var PoliticosSchema = new Schema({
	_id: Number,
	nome: String,
	nomeCompleto: String,
	bio: String,
	cargoAtual: String,
	dataNascimento: Date,
	cidadeNatal: String,
	tags: [String],
	historico: [{
		cargo: String,
		mandato: {
			inicio: Date,
			termino: Date
		},
		antecessor: String
	}],
	foto: Schema.Types.Mixed,
	slug: { type: String, lowercase: true },
	votos: { type: Number, min: 0, default: 0 },
	link: String,
	_partido: { type: Number, ref: 'Partidos' },
	_vice: { type: Number, ref: 'Politicos' }
}, options);

var PartidosSchema = new Schema({
	_id: Number,
	nome: String,
	sigla: String,
	bio: String,
	presidente: String,
	fundacao: Date,
	sede: String,
	ideologias: [String],
	link: String,
	numero: Number,
	cores: [String],
	logo: Schema.Types.Mixed,
	_politicos: [{ type: Number, ref: 'Politicos' }]
}, options);


/**
 * Plugins
 */
// Geração do Slug para o nome do político
PoliticosSchema.plugin(Slug, { source: 'nome', update: true });
PartidosSchema.plugin(Slug, { source: 'nome', update: true });

// Auto increment ID
PoliticosSchema.plugin(autoIncrement.plugin, 'Politicos');
PartidosSchema.plugin(autoIncrement.plugin, 'Partidos');

/**
 * Models
 */
mongoose.model('Politicos', PoliticosSchema);
mongoose.model('Partidos', PartidosSchema);


// Connect on mongodb
mongoose.connect('mongodb://localhost:27017/eleicoes2014');