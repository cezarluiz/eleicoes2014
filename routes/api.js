var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Politicos = mongoose.model('Politicos');
var Partidos = mongoose.model('Partidos');

/**
 * API PARA POLITICOS
 */

// Retorna todos os politicos
router.get('/politicos', function(req, res) {
	Politicos
		.find()
		.populate('_partido')
		.exec(function (err, politicos) {
			res.json({error: true});
		});
});

// Politicos específico por ID
router.get('/politicos/:id([0-9]{1,3})', function (req, res) {
	Politicos
		.findOne({
			_id: req.params.id
		})
		.populate('_partido')
		.exec(function (err, politico) {
			res.json(politico || {error: true});
		});
});

// Politicos específico por Slug
router.get('/politicos/:slug([a-z\-]+)', function (req, res) {
	Politicos
		.findOne({
			slug: req.params.slug
		})
		.populate('_partido')
		.populate('_vice')
		.exec(function (err, politico) {
			res.json(politico || {error: true});
		});
});

// Politicos por Estado
router.get('/uf/:uf([a-z]{2})', function (req, res) {
	Politicos
		.find({
			estado: req.params.uf.toUpperCase()
		})
		.populate('_partido')
		.populate('_vice')
		.exec(function (err, politicos) {
			res.json(politicos || {error: true});
		});
});

/**
 * API PARA PARTIDOS
 */
router.get('/partidos', function (req, res) {
	Partidos
		.find()
		.populate('_politicos')
		.exec(function (err, partidos) {
			res.json(partidos || {error: true});
		});
});

router.get('/partidos/:id([0-9]{1,3})', function (req, res) {
	Partidos
		.findOne({
			_id: req.params.id
		})
		.populate('_politicos')
		.exec(function (err, partidos) {
			res.json(partidos || {error: true});
		});
});

router.get('/partidos/:sigla([a-z]{2,5})', function (req, res) {
	Partidos
		.findOne({
			sigla: req.params.sigla.toUpperCase()
		})
		.populate('_politicos')
		.exec(function (err, partidos) {
			res.json(partidos || {error: true});
		});
});


/**
 * Adiciona novo politico
 */
router.post('/politicos', function (req, res) {

});



module.exports = router;
