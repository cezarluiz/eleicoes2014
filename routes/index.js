var express = require('express');
var router = express.Router();
var request = require('request');

var mongoose = require('mongoose');
var Politicos = mongoose.model('Politicos');


/* GET home page. */
router.get('/', function(req, res) {
	res.render('home/index')
});

router.get('/painel/politico/novo', function (req, res) {
	res.render('painel/politico');
});

router.post('/painel/politicos', function (req, res) {
	// Cria requisição para a API
	res.send(req.files);
});

module.exports = router;
