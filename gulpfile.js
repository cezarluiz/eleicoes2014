'use strict';
// generated on 2014-05-22 using generator-gulp-webapp 0.1.0

var gulp = require('gulp');
var rename = require('gulp-rename');

// load plugins
var $ = require('gulp-load-plugins')();

gulp.task('styles', function () {
    return gulp.src('./public/stylesheets/*.scss')
        .pipe($.rubySass({
            style: 'expanded',
            precision: 10
        }))
        .pipe($.autoprefixer('last 3 version'))
        .pipe($.size())
        .pipe($.csso())
        .pipe(rename({
        	suffix: '.min'
        }))
        .pipe(gulp.dest('./public/stylesheets'));
});

gulp.task('scripts', function () {
    return gulp.src('./public/scripts/main.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe($.uglify())
        .pipe(rename({
        	suffix: '.min'
        }))
        .pipe(gulp.dest('./public/scripts'));
});

gulp.task('html', ['styles', 'scripts'], function () {
    var jsFilter = $.filter('**/*.js');
    var cssFilter = $.filter('**/*.css');

    return gulp.src('./public/*.html')
        .pipe($.useref.assets({searchPath: '{.tmp,app}'}))
        .pipe(jsFilter)
        .pipe($.uglify())
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(cssFilter.restore())
        .pipe($.useref.restore())
        .pipe($.useref())
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});

gulp.task('images', function () {
    return gulp.src('./public/images/**/*')
        .pipe($.cache($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe($.size());
});

gulp.task('fonts', function () {
    return $.bowerFiles()
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe($.flatten())
        .pipe(gulp.dest('./public/fonts'))
        .pipe($.size());
});

gulp.task('extras', function () {
    return gulp.src(['./public/*.*', '!./public/*.html'], { dot: true })
        .pipe(gulp.dest('dist'));
});

gulp.task('clean', function () {
    return gulp.src(['.tmp', 'dist'], { read: false }).pipe($.clean());
});

gulp.task('build', ['html', 'images', 'fonts', 'extras']);

gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

gulp.task('connect', function () {
    var connect = require('connect');
    var app = connect()
        .use(connect.static('app'))
        .use(connect.static('.tmp'))
        .use(connect.directory('app'));

    require('http').createServer(app)
        .listen(9000)
        .on('listening', function () {
            console.log('Started connect web server on http://localhost:9000');
        });
});

gulp.task('serve', ['connect', 'styles'], function () {
    require('opn')('http://localhost:9000');
});

// inject bower components
gulp.task('wiredep', function () {
    var wiredep = require('wiredep').stream;

    gulp.src('./public/stylesheets/*.scss')
        .pipe(wiredep({
            directory: './public/bower_components'
        }))
        .pipe(gulp.dest('./public/styles'));

    gulp.src('./public/*.html')
        .pipe(wiredep({
            directory: './public/bower_components'
        }))
        .pipe(gulp.dest('app'));
});

gulp.task('watch', function () {
    gulp.watch('./public/stylesheets/**/*.scss', ['styles']);
    gulp.watch('./public/scripts/*.js', ['scripts']);
    gulp.watch('./public/images/**/*', ['images']);
});
