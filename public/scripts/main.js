var Eleicoes = angular.module('EleicoesApp', ['ngAnimate', 'ngRoute']);

Eleicoes.controller('EleicoesController',
[
	'$scope', '$window', '$http',
	function ($scope, $window, $http) {
		$http
			.get('/api/politicos')
			.success(function(r) {
				$scope.politicos = r;
			});
	}
]);


Eleicoes.controller('PoliticoFormController',
[
	'$scope', '$window', '$http',
	function ($scope, $window, $http) {
		$scope.cargos = [1];

		$scope.addCargo = function (e) {
			$scope.cargos.push($scope.cargos.length + 1);

			e.preventDefault();
		};
	}
]);



Eleicoes.config(
[
	'$routeProvider',
	'$locationProvider',
	function ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', function () {
				controller: 'EleicoesController'
			})
			.when('/politico/:slug', {
				controller: 'PoliticosController'
			});
	}
]);